#include "Nodo.h"
#include "Arbol.h"
#include "Ejer_1.h"
#include <string>
#include <fstream>
#include <iostream>
using namespace std;

bool encuentra(Nodo *raiz, int num){
	int turn = 0;
	bool perhaps;
	if(raiz != NULL){
		if(raiz->numero == 0){
			turn++;
		}
		perhaps = encuentra(raiz->izq,num);
		perhaps = encuentra(raiz->der,num);
	}
	if(turn == 1){
		return true;
	}else{
		return false;
	}
}


void lectura_pre(Nodo *raiz){
	if(raiz != NULL){
		if(raiz->numero < 0){
			cout<<"-("<<raiz->numero<<")-";
		}else{
			cout<<"-"<<raiz->numero<<"-";
		}
		lectura_pre(raiz->izq);
		lectura_pre(raiz->der);
	}
}

void lectura_in(Nodo *raiz){
	if(raiz != NULL){
		lectura_in(raiz->izq);
		if(raiz->numero < 0){
			cout<<"-("<<raiz->numero<<")-";
		}else{
			cout<<"-"<<raiz->numero<<"-";
		}
		lectura_in(raiz->der);
	}
}

void lectura_pos(Nodo *raiz){
	if(raiz != NULL){
		lectura_pos(raiz->izq);
		lectura_pos(raiz->der);
		if(raiz->numero < 0){
			cout<<"-("<<raiz->numero<<")-";
		}else{
			cout<<"-"<<raiz->numero<<"-";
		}
	}
}





int main (void) {
    Ejercicio e = Ejercicio();
    Arbol *arbol = e.get_arbol();
    string in;
    Nodo *nodo;
    int num, temp = 1;
    
    cout<<"Ingrese un numero entero:"<<endl;
	cout<<"   ~~~> ";
	getline(cin, in);
	num = stoi(in);
    arbol->crear(num);
    temp = e.menu();
    
    Nodo *raiz = arbol->get_raiz();
    
    
    while(temp != 0){
		
		switch(temp){
			case 1: cout<<"Ingrese un numero entero:"<<endl;
					cout<<"   ~~~> ";
					getline(cin, in);
					num = stoi(in);
					
					
					arbol->insertar(num, raiz);
					break;
					
			case 2: cout<<"Ingrese el numero entero a eliminar:"<<endl;
					cout<<"   ~~~> ";
					getline(cin, in);
					num = stoi(in);
					
					
					arbol->eliminar(num, raiz);
					break;
			
			
			
			case 3:
				break;
			
			
			case 4: lectura_pre(raiz);
					cout<<endl;
					break;
			
			case 5: lectura_in(raiz);
					cout<<endl;
					break;
			
			case 6: lectura_pos(raiz);
					cout<<endl;
					break;
			
			case 7:	arbol->crearGrafo();
				break;
			
			default: break;
		}	
						
		temp = e.menu();
		
		
	}
		
    return 0;
}
