#include "Nodo.h"
#include "Arbol.h"
using namespace std;
#include <string>
#include <iostream>
#include <fstream>

class Ejercicio {
    private:
        Arbol *arbol = NULL;

    public:
        /* constructor */
        Ejercicio() {
            this->arbol = new Arbol();
        }
        
        Arbol *get_arbol() {
            return this->arbol;
        }
        int menu(){
			string in;
			int temp, cond = 0;
			while(cond == 0){
				cout<<"Agregar otro número [1]"<<endl;
				cout<<"~~~~~~~~~~~~~~~~~~~~~~~"<<endl;
				cout<<"Eliminar número     [2]"<<endl;
				cout<<"~~~~~~~~~~~~~~~~~~~~~~~"<<endl;
				cout<<"Modificar número    [3]"<<endl;
				cout<<"~~~~~~~~~~~~~~~~~~~~~~~"<<endl;
				cout<<"Leer preorden:      [4]"<<endl;
				cout<<"~~~~~~~~~~~~~~~~~~~~~~~"<<endl;
				cout<<"Leer inorden:       [5]"<<endl;
				cout<<"~~~~~~~~~~~~~~~~~~~~~~~"<<endl;
				cout<<"Leer posorden:      [6]"<<endl;
				cout<<"~~~~~~~~~~~~~~~~~~~~~~~"<<endl;
				cout<<"Generar grafo:      [7]"<<endl;
				cout<<"~~~~~~~~~~~~~~~~~~~~~~~"<<endl;
				cout<<"Salir               [0]"<<endl;
				cout<<"-----------------------"<<endl;
				cout<<"-----------------------"<<endl;
				cout<<"Opción:  ";
				getline(cin, in);
				temp = stoi(in);
				if(temp < 0 || temp > 7){
					cout<< "opción no valida, intente nuevamente"<<endl;
				}else{
					cond = 1;
				}
			}
			return temp;				
		}		
		
		
};
