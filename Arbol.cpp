#include <iostream>
#include <string>
#include <fstream>
using namespace std;

#include "Nodo.h"
#include "Arbol.h"

Arbol::Arbol() {}

void Arbol::crear(int numero){
	Nodo *tmp;
	/*se crea el nodo*/
	tmp = new Nodo;
	
	tmp->numero = numero;
	tmp->info = to_string(numero);
	tmp->izq = NULL;
	tmp->der = NULL;
	
	this->raiz = tmp;
}

Nodo *Arbol::crear_nodo(int numero){
	Nodo *tmp;
	tmp = new Nodo;
	
	tmp->numero = numero;
	tmp->info = to_string(numero);
	tmp->izq = NULL;
	tmp->der = NULL;
	
	return tmp;
}
	
Nodo *Arbol::get_raiz(){
	return this->raiz;
}

void Arbol::insertar (int num, Nodo *raiz) {
	
    Nodo *nodo = new Nodo;
	
	/*si es menor al primero se inserta a su izquierda*/
	if(num < raiz->numero){
		if(raiz->izq == NULL){
			nodo = this->crear_nodo(num);
			raiz->izq = nodo;
			cout<<"El numero "<<num<<" fue insertado a la izquierda de "<<raiz->numero<<endl;
		}else{
			/*si el indicador esta "ocupado" se repite la función*/
			this->insertar(num, raiz->izq);
		}
	}else{
		/*si es mayor de inserta a la derecha*/
		if (num > raiz->numero){
			if(raiz->der == NULL){
				nodo = this->crear_nodo(num);
				raiz->der = nodo;		
				cout<<"El numero "<<num<<" fue insertado a la derecha de "<<raiz->numero<<endl;
			}else{
				/*si el indicador esta "ocupado" se repite la función*/
				this->insertar(num, raiz->der);		
			}
		}else{
			/*si es igual, se envia un mensaje de error*/
			if(num == raiz->numero){
				cout<<"El numero ingresado ya se encuentra en el arbol"<<endl;
		
			}
		}
	}
}



void Arbol::eliminar(int num, Nodo *raiz){
    Nodo *nodo = new Nodo;
    Nodo *temp = new Nodo;
    Nodo *temp1 = new Nodo;
    bool comp;
    
    
	if(raiz != NULL){
		if(num < raiz->numero){
			//si es menos se lanza una llamada recursiva por la izquierda
			this->eliminar(num, raiz->izq);
		}else{
			if(num > raiz->numero){
				//si es mas, la llamada se lanza por la derecha
				this->eliminar(num, raiz->der);
			}else{
				nodo = raiz;
				if(raiz->der == NULL){
					//si la coincidencia no tiene descendencia hacia la derecha
					//se lo reemplaza por su descendencia izquierda
					if(nodo->izq != NULL){
						cout<<"shoo1"<<endl;
						raiz->der = nodo->izq->der;
						raiz->numero = nodo->izq->numero;
						raiz->info = to_string(raiz->numero);
						raiz->izq = nodo->izq->izq;
					}else{
						cout<<"shoo2"<<endl;
						
						raiz = NULL;
					}
						
				}else{
					if(raiz->izq == NULL){
						//si tiene descendencia por la derecha pero no por la izquierda
						//se lo reemplaza por la descendencia derecha
						if(nodo->der != NULL){
							cout<<"shoo3a"<<endl;
							raiz = nodo->der->der;
							cout<<"shoo3b"<<endl;
							raiz->izq = nodo->der->izq;
							cout<<"shoo3e"<<endl;
							raiz->numero = nodo->der->numero;
							cout<<nodo->der->numero<<endl;
							cout<<"shoo3c"<<endl;
							raiz->info = to_string(nodo->numero);
							cout<<to_string(nodo->numero)<<endl;
							cout<<"shoo3d"<<endl;
						}else{
							cout<<"shoo4"<<endl;
							raiz = NULL;
						}
						
					}else{
						cout<<"cosa rara"<<endl;
						temp = raiz->izq;
						comp = false;
						while(temp->der != NULL){
							cout<<"times"<<endl;
							temp1 = temp;
							temp = temp->der;
							comp = true;
						}
						raiz->numero = temp->numero;
						raiz->info = temp->info;
						nodo = temp;
						if(comp == true){
							cout<<"cosafin1"<<endl;
							temp1->der = temp->izq;
						}else{
							cout<<"cosafin2"<<endl;
							raiz->izq = temp->izq;
						}
					}
				}
			}							
		}						
	}else{
		cout<<"El numero que intenta eliminar, no se encuentra"<<endl;
		cout<<"intente nuevamente"<<endl;
	}
}

/* ofstream es el tipo de dato correspondiente a archivos en cpp (el llamado es ofstream &nombre_archivo). */
void Arbol::recorrerArbol(Nodo *p, ofstream &archivo) {

  string infoTmp;
  /* Se enlazan los nodos del grafo, para diferencia entre izq y der a cada nodo se le entrega un identificador al final, siendo i: izquierda
   * y d: derecha, esto se cumplirá para los casos en donde los nodos no apunten a ningún otro (nodos finales) 
   * */
  if (p != NULL) {
	/* Por cada nodo ya sea por izq o der se escribe dentro de la instancia del archivo */  
    if (p->izq != NULL) {
      archivo<< p->info << "->" << p->izq->info << ";" << endl;
    } else {
	  infoTmp = p->info;
	  infoTmp += "i";
      archivo <<'"'<< infoTmp<<'"'<< "[shape=point]" << endl;
      archivo << p->info << "->" << '"'<< infoTmp<<'"' << ";" << endl;
    }
    
    infoTmp = p->info; 
    if (p->der != NULL) {
      archivo << p->info << "->" << p->der->info << ";" << endl;
    } else {
	  infoTmp = p->info;
	  infoTmp += "d";
      archivo << '"'<< infoTmp<<'"' << "[shape=point]" << endl;
      archivo << p->info << "->" << '"'<< infoTmp<<'"'<< ";" << endl;
    }

    /* Se realizan los llamados tanto por la izquierda como por la derecha para la creación del grafo */
    recorrerArbol(p->izq, archivo);
    recorrerArbol(p->der, archivo); 
  }
  return;
}

void Arbol::crearGrafo() {
    ofstream archivo;  
    /* Se abre/crea el archivo datos.txt, a partir de este se generará el grafo */ 
    archivo.open("grafo.txt");
    /* Se escribe dentro del archivo datos.txt "digraph G { " */ 
    archivo << "digraph G {" << endl;
    /* Se pueden cambiar los colores que representarán a los nodos, para el ejemplo el color será verde */
    archivo << "node [style=filled fillcolor=magenta];" << endl;
    /* Llamado a la función recursiva que genera el archivo de texto para creación del grafo */
    recorrerArbol(this->raiz, archivo);
    /* Se termina de escribir dentro del archivo datos.txt*/
    archivo << "}" << endl;
    archivo.close();
    
    /* genera el grafo */
    system("dot -Tpng -ografo.png grafo.txt &");
    system("eog grafo.png");
}
